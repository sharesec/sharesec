# ShareSec

Securely share self-destructing text snippets and files. Self host or use the SaaS version totally free of charge.

## Installation
1. Instructions go here

## Contributing
1. Branch / Fork.
2. Make your changes.
3. Open a merge request.
4. A maintainer will review your change.

## Local Dev
### Dependencies
1. Node.js
2. Yarn
3. AWS SAM ([Setup Instructions](https://aws.amazon.com/serverless/sam/))
4. Docker (for running lambda locally)

### Frontend
1. `cd frontend`
2. `yarn dev`

### Backend
1. `cd backend`
2. Open a new terminal tab and run `sam local start-api`
3. When you want changes to refresh you need to run `sam build` in your original terminal
