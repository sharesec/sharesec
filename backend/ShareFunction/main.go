package main

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/ventu-io/go-shortid"
	"os"
)

// The share event
type ShareEvent struct {
	TextToShare string
}

func errorResponse(err error) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode:        500,
		Body:              err.Error(),
		Headers: map[string]string{
			"Access-Control-Allow-Origin": os.Getenv("ALLOWED_ORIGIN"),
			"Access-Control-Allow-Methods": "POST,OPTIONS",
		},
	}, nil
}

func newTrue() *bool {
	b := true
	return &b
}

// Lambda Handler
func HandleRequest(ctx context.Context, event events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	eventBody := &ShareEvent{}
	err := json.Unmarshal([]byte(event.Body), eventBody)

	if err != nil {
		return errorResponse(err)
	}

	sess := session.Must(session.NewSession())

	SHARE_BUCKET := os.Getenv("SHARE_BUCKET")

	uniqueId := shortid.MustGenerate()

	s3Uploader := s3manager.NewUploader(sess)

	_, err = s3Uploader.Upload(&s3manager.UploadInput{
		Key: &uniqueId,
		Body:	bytes.NewReader([]byte(eventBody.TextToShare)),
		Bucket:	&SHARE_BUCKET,
		BucketKeyEnabled: newTrue(),
	})

	if err != nil {
		return errorResponse(err)
	}

	response, _ := json.MarshalIndent(&map[string]string{
		"id": uniqueId,
	}, "", "  ")

	return events.APIGatewayProxyResponse{
		StatusCode:        200,
		Headers: map[string]string{
			"Content-Type": "application/json",
			"Access-Control-Allow-Origin": os.Getenv("ALLOWED_ORIGIN"),
			"Access-Control-Allow-Methods": "POST,OPTIONS",
		},
		Body: string(response),
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
