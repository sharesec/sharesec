module share-func

go 1.15

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/aws/aws-sdk-go v1.36.2
	github.com/ventu-io/go-shortid v0.0.0-20201117134242-e59966efd125
)
