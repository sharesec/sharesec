package main

import (
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// Lambda Handler
func HandleRequest() (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode:        200,
		Headers: map[string]string{
			"Access-Control-Allow-Origin": os.Getenv("ALLOWED_ORIGIN"),
			"Access-Control-Allow-Methods": "GET,POST,OPTIONS",
		},
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
