package main

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"io/ioutil"
	"log"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
)

func errorResponse(err error) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode: 500,
		Body:       err.Error(),
		Headers: map[string]string{
			"Access-Control-Allow-Origin": os.Getenv("ALLOWED_ORIGIN"),
			"Access-Control-Allow-Methods": "GET,OPTIONS",
		},
	}, nil
}

// Lambda Handler
func HandleRequest(ctx context.Context, event events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	sess := session.Must(session.NewSession())

	SHARE_BUCKET := os.Getenv("SHARE_BUCKET")

	id := event.PathParameters["Id"]

	log.Println(SHARE_BUCKET)
	log.Println(id)

	s3Svc := s3.New(sess)

	obj, err := s3Svc.GetObject(&s3.GetObjectInput{
		Bucket:                     &SHARE_BUCKET,
		Key:                        &id,
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok && aerr.Code() == "NoSuchKey" {
			return events.APIGatewayProxyResponse{
				StatusCode: 404,
				Headers: map[string]string{
					"Access-Control-Allow-Origin": os.Getenv("ALLOWED_ORIGIN"),
					"Access-Control-Allow-Methods": "GET,OPTIONS",
				},
			}, nil
		} else {
			log.Println("Failed to download file")
			return errorResponse(err)
		}
	}

	blob, err := ioutil.ReadAll(obj.Body)

	if err != nil {
		return errorResponse(err)
	}

	_, err = s3Svc.DeleteObject(&s3.DeleteObjectInput{
		Bucket: &SHARE_BUCKET,
		Key: &id,
	})

	if err != nil {
		log.Println(err)
	}

	response, _ := json.MarshalIndent(&map[string]string{
		"text": string(blob),
	}, "", "  ")

	return events.APIGatewayProxyResponse{
		StatusCode:        200,
		Headers: map[string]string{
			"Content-Type": "application/json",
			"Access-Control-Allow-Origin": os.Getenv("ALLOWED_ORIGIN"),
			"Access-Control-Allow-Methods": "GET,OPTIONS",
		},
		Body:              string(response),
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
