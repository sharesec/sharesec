module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
    "semi": [1, "always"],
    "comma-dangle": [1, "only-multiline"],
    "space-before-function-paren": 1,
    "vue/singleline-html-element-content-newline": 0
  }
}
